package mantisbt.pageObjects;

import java.util.Map;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPageObjectGen {

    private Map<String, String> data;
    private WebDriver driver;
    private int timeout = 15;

    @FindBy(css = "input.button")
    @CacheLookup
    private WebElement login;

    @FindBy(css = "a[href='lost_pwd_page.php']")
    @CacheLookup
    private WebElement lostYourPassword;

    @FindBy(css = "a[href='my_view_page.php']")
    @CacheLookup
    private WebElement mantisbt;

    @FindBy(name = "secure_session")
    @CacheLookup
    private WebElement onlyAllowYourSessionToBe;

    private final String pageLoadedText = "You should disable the default 'administrator' account or change its password";

    private final String pageUrl = "/login_page.php";

    @FindBy(name = "username")
    @CacheLookup
    private WebElement username;

    @FindBy(name = "password")
    @CacheLookup
    private WebElement password;

    @FindBy(name = "perm_login")
    @CacheLookup
    private WebElement perm_login;

    @FindBy(css = "a[href='signup_page.php']")
    @CacheLookup
    private WebElement signupForANewAccount;

    public LoginPageObjectGen() {
    }

    public LoginPageObjectGen(WebDriver driver) {
        this();
        this.driver = driver;
    }

    public LoginPageObjectGen(WebDriver driver, Map<String, String> data) {
        this(driver);
        this.data = data;
    }

    public LoginPageObjectGen(WebDriver driver, Map<String, String> data, int timeout) {
        this(driver, data);
        this.timeout = timeout;
    }

    /**
     * Click on Login Button.
     *
     * @return the LoginPageObjectGen class instance.
     */
    public LoginPageObjectGen clickLoginButton() {
        login.click();
        return this;
    }

    /**
     * Click on Lost Your Password Link.
     *
     * @return the LoginPageObjectGen class instance.
     */
    public LoginPageObjectGen clickLostYourPasswordLink() {
        lostYourPassword.click();
        return this;
    }

    /**
     * Click on Mantisbt Link.
     *
     * @return the LoginPageObjectGen class instance.
     */
    public LoginPageObjectGen clickMantisbtLink() {
        mantisbt.click();
        return this;
    }

    /**
     * Click on Signup For A New Account Link.
     *
     * @return the LoginPageObjectGen class instance.
     */
    public LoginPageObjectGen clickSignupForANewAccountLink() {
        signupForANewAccount.click();
        return this;
    }

    /**
     * Fill every fields in the page.
     *
     * @return the LoginPageObjectGen class instance.
     */
    public LoginPageObjectGen fill() {
        setUsername();
        setPassword();
        setPermLogin();
        setOnlyAllowYourSessionToBeCheckboxField();
        return this;
    }

    /**
     * Fill every fields in the page and submit it to target page.
     *
     * @return the LoginPageObjectGen class instance.
     */
    public LoginPageObjectGen fillAndSubmit() {
        fill();
        return submit();
    }

    /**
     * Set Only Allow Your Session To Be Used From This Ip Address Checkbox
     * field.
     *
     * @return the LoginPageObjectGen class instance.
     */
    public LoginPageObjectGen setOnlyAllowYourSessionToBeCheckboxField() {
        if (!onlyAllowYourSessionToBe.isSelected()) {
            onlyAllowYourSessionToBe.click();
        }
        return this;
    }

    /**
     * Set default value to Remember My Login In This Browser Password field.
     *
     * @return the LoginPageObjectGen class instance.
     */
    public LoginPageObjectGen setUsername() {
        return LoginPageObjectGen.this.setUsername(data.get("USERNAME"));
    }

    /**
     * Set value to User name field.
     *
     * @param userName
     * @return the LoginPageObjectGen class instance.
     */
    public LoginPageObjectGen setUsername(String userNameStr) {
        username.sendKeys(userNameStr);
        return this;
    }

    /**
     * Set default value to Remember My Login In This Browser Password field.
     *
     * @return the LoginPageObjectGen class instance.
     */
    public LoginPageObjectGen setPassword() {
        return LoginPageObjectGen.this.setPassword(data.get("PASSWORD"));
    }

    /**
     * Set value to Remember My Login In This Browser Password field.
     *
     * @return the LoginPageObjectGen class instance.
     */
    public LoginPageObjectGen setPassword(String passwordStr) {
        password.sendKeys(passwordStr);
        return this;
    }

    /**
     * Set Remember My Login In This Browser Checkbox field.
     *
     * @return the LoginPageObjectGen class instance.
     */
    public LoginPageObjectGen setPermLogin() {
        if (!perm_login.isSelected()) {
            perm_login.click();
        }
        return this;
    }

    /**
     * Submit the form to target page.
     *
     * @return the LoginPageObjectGen class instance.
     */
    public LoginPageObjectGen submit() {
        clickLoginButton();
        return this;
    }

    /**
     * Unset Only Allow Your Session To Be Used From This Ip Address Checkbox
     * field.
     *
     * @return the LoginPageObjectGen class instance.
     */
    public LoginPageObjectGen unsetOnlyAllowYourSessionToBe() {
        if (onlyAllowYourSessionToBe.isSelected()) {
            onlyAllowYourSessionToBe.click();
        }
        return this;
    }

    /**
     * Unset Remember My Login In This Browser Checkbox field.
     *
     * @return the LoginPageObjectGen class instance.
     */
    public LoginPageObjectGen unsetPermLogin() {
        if (perm_login.isSelected()) {
            perm_login.click();
        }
        return this;
    }

    /**
     * Verify that the page loaded completely.
     *
     * @return the LoginPageObjectGen class instance.
     */
    public LoginPageObjectGen verifyPageLoaded() {
        (new WebDriverWait(driver, timeout)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getPageSource().contains(pageLoadedText);
            }
        });
        return this;
    }

    /**
     * Verify that current page URL matches the expected URL.
     *
     * @return the LoginPageObjectGen class instance.
     */
    public LoginPageObjectGen verifyPageUrl() {
        (new WebDriverWait(driver, timeout)).until(
                (ExpectedCondition<Boolean>) (WebDriver d) -> d.getCurrentUrl().contains(pageUrl));
        return this;
    }
}
