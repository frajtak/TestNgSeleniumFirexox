package test.mantisbt.recorded;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import mantisbt.pageObjects.LoginPageObject;
import org.apache.commons.io.FileUtils;
import org.testng.annotations.*;
import static org.testng.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.ITestResult;

public class LoginPageObjectTest {

    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private final StringBuffer verificationErrors = new StringBuffer();
    private LoginPageObject loginPage;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        System.setProperty("webdriver.gecko.driver", "C:\\Utils\\SeleniumDrivers\\geckodriver.exe");
        driver = new FirefoxDriver();
        baseUrl = "http://192.168.99.100:8998/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        
        loginPage = new LoginPageObject(baseUrl, driver);
    }

    @Test
    public void testLoginWithValidCredentials() throws Exception {
        loginPage.open();
        
        loginPage.setUserName("administrator");
        loginPage.setPassword("root");
        loginPage.setPermLogin();
        loginPage.login();
        
        assertTrue(isElementPresent(By.cssSelector("td.login-info-left")));
        
        driver.findElement(By.linkText("Logout")).click();
    }

    @Test
    public void testLoginWithInvalidCredentials() throws Exception {
        loginPage.open();        
        loginPage.login("administrator", "notMyPassword");
        
        String actual = driver.findElement(By.xpath("//font[@color='red'][1]")).getText();
        System.out.println(actual);
        assertEquals(actual, "Your account may be disabled or blocked or the username/password you entered is incorrect.");
    }
    
    @Test
    public void testFailingResultingInScreenshot() throws Exception {
        loginPage.open();        
        loginPage.login("administrator", "notMyPassword");
        
        String actual = driver.findElement(By.xpath("//font[@color='red'][1]")).getText();
        System.out.println(actual);
        assertEquals(actual, "You shall not pass!");
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }

    @AfterMethod
    public void takeScreenShotOnFailure(ITestResult testResult) throws IOException {
        if (testResult.getStatus() == ITestResult.FAILURE) {
            File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            String fileName = 
                            "G:\\work\\cvut\\6\\TestNgSeleniumFirexox\\output"
                            + testResult.getName()+ ".jpg";
            System.out.println("Copying screenshot from " + scrFile.getAbsolutePath() + " to " + fileName);
            FileUtils.copyFile(scrFile, new File(fileName));
        }
    }
}
