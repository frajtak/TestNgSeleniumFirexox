/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test.mantisbt.recorded;

import java.util.concurrent.TimeUnit;
import mantisbt.pageObjects.LoginPageObject;
import mantisbt.pageObjects.LoginPageObjectGen;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import static org.testng.Assert.fail;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author Karel
 */
public class LoginPageFactoryTest {

    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private final StringBuffer verificationErrors = new StringBuffer();
    private LoginPageObjectGen loginPage;

    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        System.setProperty("webdriver.gecko.driver", "C:\\Utils\\SeleniumDrivers\\geckodriver.exe");
        driver = new FirefoxDriver();
        baseUrl = "http://192.168.99.100:8998/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        // instanciace objektu
        loginPage = new LoginPageObjectGen(driver);
        // inicializace prvku
        PageFactory.initElements(driver, loginPage);
    }

    @Test
    public void testWithPageFactory() {
        driver.get(baseUrl + "login_page.php");
        loginPage
                .setUsername("administrator")
                .setPassword("root")
                .submit();
    }
    
    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }
}
